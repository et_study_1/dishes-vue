import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

import Welcome from '../views/welcome'
import Ingredient from '../views/ingredient'
import Dishes from '../views/dishes'
import Prepare from '../views/prepare'
import Order from '../views/order'

import { Message } from 'element-ui'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    redirect: '/welcome',
    children: [
      {
        path: '/welcome',
        name: 'welcome',
        component: Welcome,
      },
      {
        path: '/ingredient',
        name: 'ingredient',
        component: Ingredient,
      },
      {
        path: '/dishes',
        name: 'dishes',
        component: Dishes,
      },
      {
        path: '/prepare',
        name: 'prepare',
        component: Prepare,
      },
      {
        path: '/order',
        name: 'order',
        component: Order,
      },
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const { path } = to
  const token = sessionStorage.getItem('token')
  if (path === '/login') {
    // 在登录的状态下访问登录路由, 直接跳转到首页
    if (token) {
      return next('/')
    }
    // 在没有登录的状态下访问登录路由, 直接跳转到登录路由
    return next()
  }

  // 登录状态下 访问的不是登录路由
  if (token) {
    return next()
  }

  Message.error('请登录后操作！')
  return next('/login')
})

// 解决重复跳转错误
let routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(err => err)
}

export default router