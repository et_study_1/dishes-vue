import axios from 'axios'
import {Message} from 'element-ui'
import router from '@/router'

const request = axios.create({
  baseURL: process.env.VUE_APP_GATEWAY_URL
})

// 白名单
const whiteList = ['/dishes-sys/user/login', '/dishes-sys/user/reg', '/code/getCode']

/* 请求拦截器 */
request.interceptors.request.use(config => {
  const {url} = config
  if (whiteList.indexOf(url) >= 0) {
    return config
  }

  /* 如果不是白名单的请求, 则设置请求头的token信息 */
  const token = sessionStorage.getItem('token')
  config.headers['Authorization'] = token
  return config
}, e => {
  return Promise.reject(e)
})

/* axios响应拦截器 */
request.interceptors.response.use(resp => {
  // const data = resp.data
  const {data} = resp

  const url = resp.config.url
  if (whiteList.indexOf(url) >= 0) {
    return data
  }

  // 如果后端返回403, 表示token肯定有问题, 在让用户重新登录
  if (data.code === 403) {
    sessionStorage.clear()
    Message.error(data.message + '请重新登录，两秒跳转！')
    setTimeout(() => {
      router.push('/login')
    }, 2000)
    return Promise.reject(data.message)
  }
  return data
}, e => {
  return Promise.reject(e)
})

export default request