import request from '@/util/request'
import qs from 'qs'

export function login(loginForm) {
  return request({
    url: '/dishes-sys/user/login',
    method: 'post',
    data: qs.stringify(loginForm) // 参数变为form参数类型
  })
}