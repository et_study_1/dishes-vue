import request from '@/util/request'

export function getDictList(type) {
  return request({
    // url: '/dishes-sys/dict/list?type=' + type,
    // url: `/dishes-sys/dict/list?type=${type}`,
    url: '/dishes-sys/dict/list',
    params: {
      type: type
    },
    method: 'get'
  })
}