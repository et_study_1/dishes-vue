import request from '@/util/request'

export function addDishes(dishes) {
  return request({
    url: '/dishes-sys/dishes',
    method: 'post',
    data: dishes
  })
}

export function getDishesList(pageNumber, pageSize, dishes) {
  return request({
    url: `/dishes-sys/dishes?pageNumber=${pageNumber}&pageSize=${pageSize}`,
    method: 'get',
    params: dishes
  })
}

export function updateDishes(dishes) {
  return request({
    url: '/dishes-sys/dishes',
    method: 'put',
    data: dishes
  })
}

export function deleteDish(id) {
  return request({
    url: `/dishes-sys/dishes?id=${id}`,
    method: 'delete',
  })
}