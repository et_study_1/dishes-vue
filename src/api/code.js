import request from '@/util/request'

export function getCode() {
  return request({
    url: '/code/getCode',
    method: 'get'
  })
}