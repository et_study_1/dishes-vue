import request from '@/util/request'

/**
 * 添加食材
 */
export function addIngredient(ingredient) {
  return request({
    url: '/dishes-sys/ingredient',
    method: 'post',
    data: ingredient
  })
}

/**
 * 分页查询
 */
export function getIngredientList(pageNumber, pageSize, queryForm) {
  return request({
    url: `/dishes-sys/ingredient?pageNumber=${pageNumber}&pageSize=${pageSize}`,
    method: 'get',
    params: queryForm
  })
}

export function updateIngredient(id, ingredient) {
  return request({
    url: `/dishes-sys/ingredient/${id}`,
    method: 'put',
    data: ingredient
  })
}

export function deleteIngredient(id) {
  return request({
    url: `/dishes-sys/ingredient/${id}`,
    method: 'delete',
  })
}

export function getIngredientNameList() {
  return request({
    url: '/dishes-sys/ingredient/nameList',
    method: 'get'
  })
}
