import request from '@/util/request'

export function addPrepareDishes(dishes) {
  return request({
    url: '/dishes-sys/prepare/add',
    method: 'post',
    data: dishes
  })
}

export function getPrepareDishesList() {
  return request({
    url: '/dishes-sys/prepare/list',
    method: 'get'
  })
}

export function deletePrepareDishes(id) {
  return request({
    url: `/dishes-sys/prepare/${id}`,
    method: 'delete'
  })
}