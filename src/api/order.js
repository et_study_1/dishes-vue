import request from '@/util/request'

export function createOrder(order) {
  return request({
    url: '/dishes-sys/order',
    method: 'post',
    data: order
  })
}

export function getOrderList(){
  return request({
    url: '/dishes-sys/order/list',
    method: 'get'
  })
}

export function getOrderInfo(id){
  return request({
    url: '/dishes-sys/order/getOrderInfo',
    method: 'get',
    params: {
      orderNo: id
    }
  })
}