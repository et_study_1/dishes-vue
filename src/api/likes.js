import request from "@/util/request";

export function like(dishesId) {
  return request({
    url: `/dishes-sys/likes/dishes/${dishesId}`,
    method: 'get'
  })
}