import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

/* 导入ElementUI */
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)

// 为Vue对象添加base_url属性
Vue.prototype.base_url = process.env.VUE_APP_GATEWAY_URL

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
